package com.youthcool.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import springfox.documentation.annotations.ApiIgnore;

@RestController
public class MainController
{
	@ApiIgnore
	@RequestMapping(value="/hello",method=RequestMethod.GET)
	 public String main(){
        return "hello";
    }
}
